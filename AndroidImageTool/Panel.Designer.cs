﻿namespace AndroidImageTool
{
  partial class Panel
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Panel));
      this.grpImage = new System.Windows.Forms.GroupBox();
      this.lblSourceInfo = new System.Windows.Forms.Label();
      this.pbSource = new System.Windows.Forms.PictureBox();
      this.grpSource = new System.Windows.Forms.GroupBox();
      this.cmbDPI = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.grpOutput = new System.Windows.Forms.GroupBox();
      this.btnGenerate = new System.Windows.Forms.Button();
      this.txtOutputPath = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.txtOutputName = new System.Windows.Forms.TextBox();
      this.grpImage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbSource)).BeginInit();
      this.grpSource.SuspendLayout();
      this.grpOutput.SuspendLayout();
      this.SuspendLayout();
      // 
      // grpImage
      // 
      this.grpImage.Controls.Add(this.lblSourceInfo);
      this.grpImage.Controls.Add(this.pbSource);
      this.grpImage.Location = new System.Drawing.Point(12, 12);
      this.grpImage.Name = "grpImage";
      this.grpImage.Size = new System.Drawing.Size(334, 369);
      this.grpImage.TabIndex = 0;
      this.grpImage.TabStop = false;
      this.grpImage.Text = "DnD the source imge here";
      // 
      // lblSourceInfo
      // 
      this.lblSourceInfo.AutoSize = true;
      this.lblSourceInfo.Location = new System.Drawing.Point(6, 346);
      this.lblSourceInfo.Name = "lblSourceInfo";
      this.lblSourceInfo.Size = new System.Drawing.Size(65, 13);
      this.lblSourceInfo.TabIndex = 1;
      this.lblSourceInfo.Text = "Source size:";
      // 
      // pbSource
      // 
      this.pbSource.Location = new System.Drawing.Point(6, 17);
      this.pbSource.Name = "pbSource";
      this.pbSource.Size = new System.Drawing.Size(320, 320);
      this.pbSource.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pbSource.TabIndex = 0;
      this.pbSource.TabStop = false;
      this.pbSource.DragDrop += new System.Windows.Forms.DragEventHandler(this.pbSource_DragDrop);
      this.pbSource.DragEnter += new System.Windows.Forms.DragEventHandler(this.pbSource_DragEnter);
      // 
      // grpSource
      // 
      this.grpSource.Controls.Add(this.cmbDPI);
      this.grpSource.Controls.Add(this.label1);
      this.grpSource.Enabled = false;
      this.grpSource.Location = new System.Drawing.Point(12, 391);
      this.grpSource.Name = "grpSource";
      this.grpSource.Size = new System.Drawing.Size(334, 60);
      this.grpSource.TabIndex = 1;
      this.grpSource.TabStop = false;
      this.grpSource.Text = "Select source options";
      // 
      // cmbDPI
      // 
      this.cmbDPI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbDPI.FormattingEnabled = true;
      this.cmbDPI.Items.AddRange(new object[] {
            "M - MDPI",
            "H - HDPI",
            "X - XHDPI",
            "XX - XHDPI",
            "XXX - XXXHDPI"});
      this.cmbDPI.Location = new System.Drawing.Point(205, 22);
      this.cmbDPI.Name = "cmbDPI";
      this.cmbDPI.Size = new System.Drawing.Size(121, 21);
      this.cmbDPI.TabIndex = 1;
      this.cmbDPI.SelectedIndexChanged += new System.EventHandler(this.cmbDPI_SelectedIndexChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(6, 25);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(95, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Source DPI group:";
      // 
      // grpOutput
      // 
      this.grpOutput.Controls.Add(this.txtOutputName);
      this.grpOutput.Controls.Add(this.label3);
      this.grpOutput.Controls.Add(this.btnGenerate);
      this.grpOutput.Controls.Add(this.txtOutputPath);
      this.grpOutput.Controls.Add(this.label2);
      this.grpOutput.Enabled = false;
      this.grpOutput.Location = new System.Drawing.Point(12, 460);
      this.grpOutput.Name = "grpOutput";
      this.grpOutput.Size = new System.Drawing.Size(334, 110);
      this.grpOutput.TabIndex = 2;
      this.grpOutput.TabStop = false;
      this.grpOutput.Text = "Output";
      // 
      // btnGenerate
      // 
      this.btnGenerate.Enabled = false;
      this.btnGenerate.Location = new System.Drawing.Point(251, 72);
      this.btnGenerate.Name = "btnGenerate";
      this.btnGenerate.Size = new System.Drawing.Size(75, 23);
      this.btnGenerate.TabIndex = 3;
      this.btnGenerate.Text = "Generate";
      this.btnGenerate.UseVisualStyleBackColor = true;
      this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
      // 
      // txtOutputPath
      // 
      this.txtOutputPath.Location = new System.Drawing.Point(9, 43);
      this.txtOutputPath.Name = "txtOutputPath";
      this.txtOutputPath.Size = new System.Drawing.Size(317, 20);
      this.txtOutputPath.TabIndex = 1;
      this.txtOutputPath.TextChanged += new System.EventHandler(this.txtOutput_TextChanged);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(6, 23);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(79, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "Drawable path:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(6, 77);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(84, 13);
      this.label3.TabIndex = 4;
      this.label3.Text = "Drawable name:";
      // 
      // txtOutputName
      // 
      this.txtOutputName.Location = new System.Drawing.Point(96, 74);
      this.txtOutputName.Name = "txtOutputName";
      this.txtOutputName.Size = new System.Drawing.Size(100, 20);
      this.txtOutputName.TabIndex = 5;
      this.txtOutputName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOutputName_KeyDown);
      // 
      // Panel
      // 
      this.AllowDrop = true;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(354, 585);
      this.Controls.Add(this.grpOutput);
      this.Controls.Add(this.grpSource);
      this.Controls.Add(this.grpImage);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.Name = "Panel";
      this.Text = "Android image utility";
      this.Load += new System.EventHandler(this.Panel_Load);
      this.grpImage.ResumeLayout(false);
      this.grpImage.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbSource)).EndInit();
      this.grpSource.ResumeLayout(false);
      this.grpSource.PerformLayout();
      this.grpOutput.ResumeLayout(false);
      this.grpOutput.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox grpImage;
    private System.Windows.Forms.PictureBox pbSource;
    private System.Windows.Forms.GroupBox grpSource;
    private System.Windows.Forms.ComboBox cmbDPI;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox grpOutput;
    private System.Windows.Forms.Button btnGenerate;
    private System.Windows.Forms.TextBox txtOutputPath;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label lblSourceInfo;
    private System.Windows.Forms.TextBox txtOutputName;
    private System.Windows.Forms.Label label3;

  }
}

