﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using FreeImageAPI;

namespace AndroidImageTool
{
  public partial class Panel : Form, IDropTarget
  {
    const double FACTOR_MDPI = 1;
    const double FACTOR_HDPI = 1.5;
    const double FACTOR_XHDPI = 2;
    const double FACTOR_XXHDPI = 3;
    const double FACTOR_XXXHDPI = 4;

    int mSourceWidth;
    int mSourceHeight;
    double mSourceDPGroup;
    FIBITMAP mImage;

    public Panel()
    {
      InitializeComponent();
      pbSource.AllowDrop = true;
    }

    private void Panel_Load(object sender, EventArgs e)
    {
    }

    private void pbSource_DragEnter(object sender, DragEventArgs e)
    {
      string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
      string file = files[0].ToLower();
      if (file.EndsWith("png"))
        e.Effect = DragDropEffects.Move;
    }

    private void pbSource_DragDrop(object sender, DragEventArgs e)
    {
      string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
      string file = files[0].ToLower();
      pbSource.Image = Image.FromFile(file);
      mImage = FreeImage.Load(FREE_IMAGE_FORMAT.FIF_PNG, file, 0);

      mSourceWidth = (int)FreeImage.GetWidth(mImage);
      mSourceHeight = (int)FreeImage.GetHeight(mImage);
      lblSourceInfo.Text = "Size: " + mSourceWidth.ToString() + " x " + mSourceHeight.ToString();
      txtOutputName.Text = Path.GetFileNameWithoutExtension(file);

      grpSource.Enabled = true;
      if (cmbDPI.SelectedIndex != -1)
        cmbDPI_SelectedIndexChanged(sender, null);
    }
    private void cmbDPI_SelectedIndexChanged(object sender, EventArgs e)
    {
      grpOutput.Enabled = true;

      if (cmbDPI.SelectedIndex == 1)
        mSourceDPGroup = FACTOR_HDPI;
      else if (cmbDPI.SelectedIndex == 2)
        mSourceDPGroup = FACTOR_XHDPI;
      else if (cmbDPI.SelectedIndex == 3)
        mSourceDPGroup = FACTOR_XXHDPI;
      else if (cmbDPI.SelectedIndex == 4)
        mSourceDPGroup = FACTOR_XXXHDPI;
      else
        mSourceDPGroup = FACTOR_MDPI;

      StringBuilder ss = new StringBuilder();
      ss.Append("Size: ");
      ss.Append(mSourceWidth.ToString());
      ss.Append(" x ");
      ss.Append(mSourceHeight.ToString());

      ss.Append(" DiP: ");
      ss.Append(round(mSourceWidth / mSourceDPGroup));
      ss.Append(" x ");
      ss.Append(round(mSourceHeight / mSourceDPGroup));
      ss.Append(" (");
      ss.AppendFormat("{0:N2}", mSourceWidth / mSourceDPGroup / 160 / 0.394);
      ss.Append("cm x ");
      ss.AppendFormat("{0:N2}", mSourceHeight / mSourceDPGroup / 160 / 0.394);
      ss.Append("cm)");

      lblSourceInfo.Text = ss.ToString();
    }

    private void txtOutput_TextChanged(object sender, EventArgs e)
    {
      btnGenerate.Enabled = true;
    }

    private int round(double value)
    {
      return (int)(value + 0.5);
    }

    private void export(double output_dpi, string output_dpi_name)
    {
      int width = round(mSourceWidth / mSourceDPGroup * output_dpi);
      int height = round(mSourceHeight / mSourceDPGroup * output_dpi);
      String file = Path.Combine(txtOutputPath.Text, "drawable-" + output_dpi_name, txtOutputName.Text + ".png");
      FIBITMAP result = FreeImage.Rescale(mImage, width, height, FREE_IMAGE_FILTER.FILTER_BSPLINE);
      FreeImage.Save(FREE_IMAGE_FORMAT.FIF_PNG, result, file, FREE_IMAGE_SAVE_FLAGS.PNG_Z_DEFAULT_COMPRESSION);
      FreeImage.Unload(result);
    }

    private void btnGenerate_Click(object sender, EventArgs e)
    {
      export(FACTOR_MDPI, "mdpi");
      export(FACTOR_HDPI, "hdpi");
      export(FACTOR_XHDPI, "xhdpi");
      export(FACTOR_XXHDPI, "xxhdpi");

      FreeImage.Unload(mImage);
      MessageBox.Show("Done.");

      mSourceWidth = 0;
      mSourceHeight = 0;
      mSourceDPGroup = 0;
      mImage = FIBITMAP.Zero;
      txtOutputName.Text = "";
      lblSourceInfo.Text = "Source size:";
      pbSource.Image = null;
    }

    private void txtOutputName_KeyDown(object sender, KeyEventArgs e)
    {
      if(e.KeyCode == Keys.Enter)
      {
        e.Handled = true;
        e.SuppressKeyPress = true;
        btnGenerate_Click(sender, null);
      }
    }
  }
}
